#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Note, aspects of these scripts are based on the example clients provided in
# https://github.com/OpenSILEX/phis-ws-clients/tree/master/python

import requests, json, csv, os.path, hashlib, pytz
import ntpath
import mimetypes
import datetime
import urllib
import pprint
import phis
import settings

import create_groups
import create_project

session = phis.create_session(settings.phis_host_url, 
                              settings.username, 
                              settings.password)
print("Session created:")
print(session)
    
# Post experiments
experiments = [
  {
    "startDate": "2018-02-01",
    "endDate": "2018-03-31",
    "field": "Greenhouse Bleiswijk",
    "campaign": "SPEC_CAM_EVAL_FX10",
    "place": "Spectral lab Wageningen",
    "alias": "SPEC_CAM_EVAL_FX10",
    "comment": "Data collection to test the capabilities of the Specim FX10 on tomato plants.",
    "keywords": "hyperspectral, camera, specim, fx10, powdery mildew, tomato",
    "objective": "Collect images of tomato plants infected with powdery mildew using a Specim FX10 camera.",
    "cropSpecies": "tomato",
    "projectsUris": [
      create_project.project_uri
    ],
    "groupsUris": [
      create_groups.group_uri
    ],
    "contacts": [
      {
        "email": "admin@opensilex.org", # Similar to create_project, we use the admin user, see note with real metadata below.
        "firstName": "Admin",
        "familyName": "OpenSILEX",
        "type": "string"
      }
    ]
  }
]

# Real contact metadata
#   "contacts": [
#      {
#        "email": "gerrit.polder@wur.nl",
#        "firstName": "Gerrit",
#        "familyName": "Polder",
#        "type": "Project contact"
#      }


print()
print("=====================")
print("EXPERIMENTS:")
print()

# In create_project, we explicitly did the check for existing projects, in
# the below call, we shorten this procedure by using the method define_post_or_put
# implemented in phis.py . A call to this post_or_put_experiment should now
# be interpreted just like the larger procedure in create_project.py
# The interested reader should refer to define_post_or_put_experiment in
# phis.py. However, the reader may safely simply note that the general procedure
# of create_project.py is being followed.

post_or_put_experiment = phis.define_post_or_put(getter=phis.get_experiments,
                                                 poster=phis.post_experiments,
                                                 putter=phis.put_experiments)

experiment_uris = []
for experiment in experiments:
    query_params = {"alias" : experiment["alias"]}
    experiment_uris.append(post_or_put_experiment(session, query_params, experiment, verbose=True)["uri"])

print("URIS: ")
for uri in experiment_uris:
    print(uri)
