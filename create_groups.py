#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Note, aspects of these scripts are based on the example clients provided in
# https://github.com/OpenSILEX/phis-ws-clients/tree/master/python


import requests, json, csv, os.path, hashlib, pytz
import ntpath
import mimetypes
import datetime
import urllib
import pprint
import phis
import settings

session = phis.create_session(settings.phis_host_url, 
                              settings.username, 
                              settings.password)
print("Session created:")
print(session)

groups = []
groups.append(
  {
    "name": "AgroFoodRobotics",
    "level": "Owner",
    "description": "Located at WUR",
    "usersEmails": [
      "admin@opensilex.org"
    ]
  }
)

print()
print("=====================")
print("GROUPS:")
print()
print()

# In create_project, we explicitly did the check for existing projects, in
# the below call, we shorten this procedure by using the method define_post_or_put
# implemented in phis.py . A call to this post_or_put_group should now
# be interpreted just like the larger procedure in create_project.py
# The interested reader should refer to define_post_or_put_experiment in
# phis.py. However, the reader may safely simply note that the general procedure
# of create_project.py is being followed.
post_or_put_group = phis.define_post_or_put(getter=phis.get_groups,
                                                 poster=phis.post_groups,
                                                 putter=phis.put_groups)

group_uris = []
for group in groups:
    query_params = {"name": group["name"]}
    group_uris.append(post_or_put_group(session, query_params, group)["uri"])

print("URIS: ")
for uri in group_uris:
    print(uri)
    
group_uri = group_uris[0]
