#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Note, aspects of these scripts are based on the example clients provided in
# https://github.com/OpenSILEX/phis-ws-clients/tree/master/python


import requests, json, csv, os.path, hashlib, pytz
import ntpath
import mimetypes
import datetime
import urllib
import pprint
import phis

import io
import zipfile
import os
import os.path

import settings
import create_project
import create_experiment
import create_provenance
import create_sensors
import create_scientific_objects

hyperspectral_data_root = os.path.join(settings.dataset_directory, "zips")

session = phis.create_session(settings.phis_host_url, 
                              settings.username, 
                              settings.password)
print("Session created:")
print(session)# In[8]:  


print("POSTING IMAGES")
print("========================")


RGB_image_type = "http://www.opensilex.org/vocabulary/oeso#RGBImage"
Datafile_type = "http://www.opensilex.org/vocabulary/oeso#DataFile"
VNIR_image_type = "http://www.opensilex.org/vocabulary/oeso#VNIRImage"

type_uri = VNIR_image_type

def matches_extension(path, extension, case_sensitive=False):
    if (not case_sensitive):
        path = path.upper()
        extension = extension.upper()
    return path.endswith(extension)
           
def get_all_files(path, filename_extension, case_sensitive=False):
    all_files = []
       
    for dirpath, dirnames, filenames in os.walk(path):
        matching_files = [f for f in filenames if matches_extension(f, filename_extension, case_sensitive=case_sensitive)]
        matching_paths = [os.path.join(dirpath, f) for f in matching_files]
        all_files.extend(matching_paths)
    return all_files

def date_from_path(path):
    # This function is specific to the dataset. Filenames
    basename = os.path.basename(path)
    items = basename.split("FX10")[1] # the date comes after FX10 in the filename
    items = items.replace("_","") # get rid of underscore
    items = items.split(".")[0] # get rid of extension
    return items

def sample_labels_from_path(path):
    # This function is specific to the dataset. Filenames
    basename = os.path.basename(path)
    items = basename.split("FX10")[0]
    items = items.replace("_","").strip()
    items = items.split(" ")
    return items

def alias_from_label(label):
    return "SPEC_CAM_EVAL_" + label

def scientific_object_uris_from_labels(labels):
    aliases = [alias_from_label(l) for l in labels]
    iri_dict = create_scientific_objects.scientific_object_iris
    return [iri_dict[a] for a in aliases]

def make_concerned_item(uri, type_uri):
    return {'uri': uri,
            'typeURI': type_uri}

def concerned_items_from_uris(uris, type_uri):
    return [make_concerned_item(uri, type_uri) for uri in uris]

def build_concerned_items_dict(paths, type_uri):
    labels = [sample_labels_from_path(f) for f in paths]
    uris = [scientific_object_uris_from_labels(l) for l in labels]
    concerned_items_lists = [concerned_items_from_uris(c, type_uri) for c in uris]
    return dict(zip(paths, concerned_items_lists))
       
def format_date(datetosend):
    tz = pytz.timezone(settings.timezone)
    datetosend = datetosend.replace(tzinfo=tz)
    dateforwebservice = datetosend.strftime("%Y-%m-%dT%H:%M:%S%z")
    return dateforwebservice



def post_hyperspectral_data(session, root, provenance_uri, sensor_uri, type_uri, experiment_uri, project_uri):
    file_dict = build_concerned_items_dict(get_all_files(root,"zip"), type_uri)
    
    for file_path, concerned_items in file_dict.items():
        date = date_from_path(file_path)                
        collection_datetime = datetime.datetime.strptime(date,"%Y-%m-%d")
        date_for_webservice = format_date(collection_datetime)
                
        metadata = {"sensor" : sensor_uri, 
                    "project" : project_uri,
                    "experiment" : experiment_uri,
                    "filename" : os.path.basename(file_path)}

        search_dict = {
                "rdfType" : type_uri,
                "startDate" : date_for_webservice,
                "endDate" : date_for_webservice,
                "provenanceUri": provenance_uri,
                "concernedItems" : concerned_items[0]["uri"]
          }
        
        result = phis.get_data_file_search(session, query_params=search_dict)

        if (len(result["result"]["data"]) > 0):
            print("Skipping already uploaded:" + os.path.basename(file_path))      
            continue
        else:
            print("Uploading:" + os.path.basename(file_path))      
      
        phis.post_file(session, 
                       file_path, 
                       type_uri, 
                       provenance_uri, 
                       concerned_items, 
                       metadata, 
                       date_for_webservice)

                
post_hyperspectral_data(session, 
              hyperspectral_data_root,
              create_provenance.provenance_uris[0], 
              create_sensors.sensor_uris[0],
              type_uri,
              create_experiment.experiment_uris[0],
              create_project.project_uri)

