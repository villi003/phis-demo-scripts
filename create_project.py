#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Note, aspects of these scripts are based on the example clients provided in
# https://github.com/OpenSILEX/phis-ws-clients/tree/master/python


import requests, json, csv, os.path, hashlib, pytz
import ntpath
import mimetypes
import datetime
import urllib
import pprint
import phis
import settings

# These lines get an authentication token to access
# PHIS services using a username and password.
# Lookin in phis.py for the implementation of the
# authentication procedure (which follows the tutorial
# text). If the PHIS installation uses a different
# URL, username or password, change the values in
# settings.py
session = phis.create_session(settings.phis_host_url, 
                              settings.username, 
                              settings.password)
print("Session created:")
print(session)

#===================================================================

# Metadata for project
project = {
    "name": "Spectral Camera Evaluation",
    "acronyme": "SPEC_CAM_EVAL",
    "subprojectType": "",
    "financialSupport": "Wageningen University and Research",
    "financialName": "financial name",
    "dateStart": "2017-12-01",
    "dateEnd": "2018-12-31",
    "keywords": "hyperspectral, camera, powdery mildew, tomato",
    "description": "This project collects data from a number of different hyperspectral cameras in order to compare data quality, as well as to compare subsequent performance on classification tasks.",
    "objective": "Compare the capabilities of a number of hyperspectral cameras, particularly with respect to a subsequent classification task to detect powdery mildew.",
    "parentProject": "None",
    "website": "http://www.wur.nl",
    "contacts": [
      {
        "email": "admin@opensilex.org",  # Note this user MUST exist, see note below
        "firstName": "Admin",
        "familyName": "OpenSILEX",
        "type": "string"
      }
    ]
  }

# In the above, we have indicated the contact as the admin user.
# In reality, this should be the project contact person. 
# However, this user must also be a registered user of the PHIS
# system. Creating such a user is typically the responsibility
# of the administrator. The true metadata for this case study
# is provided below for reference purposes.
    
#   "contacts": [
#      {
#        "email": "gerrit.polder@wur.nl",
#        "firstName": "Gerrit",
#        "familyName": "Polder",
#        "type": "Project contact"
#      }
  
print()
print("=====================")
print("PROJECTS:")
print()

# The following code uses phis.get/post/push_projects to create project 
# as entities in the PHIS database. These functions are defined in phis.py
# get_projects = define_phis_request("GET", "projects")
# post_projects = define_phis_request("POST", "projects")
# put_projects = define_phis_request("PUT", "projects", postprocess_response=lambda x:x) # No response, so should parse as JSON
#
# The interested Python using readers should look at define_phis_request 
# in phis.py this is used to define many of the PHIS calls, since they 
# mostly follow the same structure. Users of other languages should check 
# their own language's facilities for performing web requests.

# IN THE FOLLOWING, NOTE: PHIS will happily create duplicate entries if you POST the same entity twice, so it is important to check
# for existing entries beforehand. Otherwise you run the risk of attaching aspects of the datasets to different
# items that should actually be the same entity.

existing_projects = phis.get_projects(session, query_params={"name" : project["name"]})["result"]["data"]
print("Matching existing projects:")
print(existing_projects)

assert len(existing_projects) <= 1, "There should be either 0 or exactly 1 match for the project in the repository, but there were %d. Did someone post the project twice?" % (len(existing_projects),)
    
if (len(existing_projects) == 0):
    print("No existing project matches, we POST the project to create it.")
    result = phis.post_projects(session, data = [project])    
else:
    print("A project matches, we use PUT to update it.")
    project_uri = existing_projects[0]["uri"]
    project["uri"] = project_uri
    result = phis.put_projects(session, data = [project])

print("Result = ", str(result))

# Now we double check that the project has been inserted
existing_projects = phis.get_projects(session, query_params={"name": project["name"]})["result"]["data"]

assert len(existing_projects) == 1, "There should be exactly 1 match for the project in the repository after posting, did the post call fail?"
project_uri = existing_projects[0]["uri"]

print("PROJECT URI: " + project_uri)

