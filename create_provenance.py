#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Note, aspects of these scripts are based on the example clients provided in
# https://github.com/OpenSILEX/phis-ws-clients/tree/master/python


import requests, json, csv, os.path, hashlib, pytz
import ntpath
import mimetypes
import datetime
import urllib
import pprint
import phis
import settings

session = phis.create_session(settings.phis_host_url, 
                              settings.username, 
                              settings.password)
print("Session created:")
print(session)

provenances = []
# Provenance metadata
provenances.append({
    "label": "SPECTRAL_CAM_EVAL_2019",
    "comment": "Contact person Gerrit Polder (gerrit.polder@wur.nl). Citation: Malounas, I. (2019), Evaluation of multispectral and hyperspectral cameras for detection of powdery mildew on tomato plants using convolutional neural networks, MSc Thesis Farm Technology, Wageningen University and Research.",
    "metadata": {},
})

print()
print("=====================")
print("PROVENANCE:")
print()
print()

# In create_project, we explicitly did the check for existing projects, in
# the below call, we shorten this procedure by using the method define_post_or_put
# implemented in phis.py . A call to this post_or_put_provenance should now
# be interpreted just like the larger procedure in create_project.py
# The interested reader should refer to define_post_or_put_experiment in
# phis.py. However, the reader may safely simply note that the general procedure
# of create_project.py is being followed.
post_or_put_provenance = phis.define_post_or_put(getter=phis.get_provenances,
                                                 poster=phis.post_provenances,
                                                 putter=phis.put_provenances)

provenance_uris = []
for provenance in provenances:
    query_params = {"label": provenance["label"]}
    provenance_uris.append(post_or_put_provenance(session, query_params, provenance)["uri"])

print("URIS: ")
for uri in provenance_uris:
    print(uri)
    
provenance_uri = provenance_uris[0]