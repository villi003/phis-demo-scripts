#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Note, aspects of these scripts are based on the example clients provided in
# https://github.com/OpenSILEX/phis-ws-clients/tree/master/python


scientific_object_type ="http://www.opensilex.org/vocabulary/oeso#Plant"

import requests, json, csv, os.path, hashlib, pytz
import ntpath
import mimetypes
import datetime
import urllib
import pprint
import phis
import settings

import create_experiment
experiment_uri = create_experiment.experiment_uris[0]


session = phis.create_session(settings.phis_host_url, 
                              settings.username, 
                              settings.password)
print("Session created:")
print(session)

print("CREATE SCIENTIFIC OBJECTS")
print("=========================================")


import copy

def update_scientific_object(entity, label):
    entity = copy.deepcopy(entity)
    entities = [entity]
    phis_getter = phis.define_phis_request("GET", "scientificObjects")
    phis_poster = phis.define_phis_request("POST", "scientificObjects")
    phis_putter = phis.define_phis_request("PUT", "scientificObjects/%s/%s", ["uri", "experiment"], postprocess_response=lambda x:x) # No response, so should parse as JSON

    existing = phis_getter(session, query_params={"alias" : label})["result"]["data"]     
    existing = [ex for ex in existing if ex["label"] == label] # only accept exact label matches
            
    assert len(existing) <= 1, "There should be either 0 or exactly 1 match for the entity in the repository, but there were %d. Did someone post it twice?" % (len(existing),)
    
    if (len(existing) == 0):
        print("No existing entity matches, we POST the object to create it.")
        phis_poster(session, data = entities)
    else:
        print("An entity matches, we use PUT to update it.")
        uri = existing[0]["uri"]
        phis_putter(session, uri=uri, experiment=entity["experiment"], data = entities[0]) # We do not send a list of entities
    
    existing = phis_getter(session, query_params={"alias": label})["result"]["data"]
    existing = [ex for ex in existing if ex["label"] == label] # only accept exact label matches
    
    #print(existing)
    assert len(existing) == 1, "There should be exactly 1 match for the experiments in the repository after posting, did the post call fail?"
    uri = existing[0]["uri"]
    
    return uri

def create_plant_object(label):
    global experiment_uri, scientific_object_type
     
    scientific_object = {
         "rdfType": scientific_object_type,
         "experiment": experiment_uri,
         #"isPartOf": "http://www.opensilex.org/opensilex/2019/o19000012",##"string",
         #"year": "2017",
         "properties": [
               {
                 "rdfType": None,
                 "relation": "http://www.w3.org/2000/01/rdf-schema#label",
                 "value": label
               }
           ]
         }
       
    return update_scientific_object(scientific_object, label)

def create_all_scientific_objects():
    scientific_object_iris = {}
    # We only upload 2 control and 2 diseased plant images
    # in the demonstration dataset.
    for n in range(1,3):
        for prefix in ["C", "D"]:
            scientific_object_alias = "SPEC_CAM_EVAL_%s%d" % (prefix, n)
            print("Creating/Updating: " + scientific_object_alias)
            scientific_object_uri = create_plant_object(scientific_object_alias)
            scientific_object_iris[scientific_object_alias] = scientific_object_uri
    return scientific_object_iris

scientific_object_iris = create_all_scientific_objects()