#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Note, aspects of these scripts are based on the example clients provided in
# https://github.com/OpenSILEX/phis-ws-clients/tree/master/python


import requests, json, csv, os.path, hashlib, pytz
import ntpath
import mimetypes
import datetime
import urllib
import pprint
import phis
import settings

session = phis.create_session(settings.phis_host_url, 
                              settings.username, 
                              settings.password)
print("Session created:")
print(session)

# Sensor metadata
sensors = []
sensors.append({ 'rdfType': 'http://www.opensilex.org/vocabulary/oeso#HyperspectralCamera',
     'label': 'SPECIM_FX10',
     'brand': 'SPECIM',
     'model': "FX10",
     'serialNumber': None,
     'inServiceDate': '2019-01-01',
     'dateOfPurchase': None,
     'dateOfLastCalibration': None,
     'personInCharge': 'admin@opensilex.org',
     'variables': {}})

print()
print("=====================")
print("SENSORS:")
print()
print()

# In create_project, we explicitly did the check for existing projects, in
# the below call, we shorten this procedure by using the method define_post_or_put
# implemented in phis.py . A call to this post_or_put_sensors should now
# be interpreted just like the larger procedure in create_project.py
# The interested reader should refer to define_post_or_put_experiment in
# phis.py. However, the reader may safely simply note that the general procedure
# of create_project.py is being followed.
# DO NOTE: Posting of sensor PROFILES needs a custom approach, which is
# shown later in this script.

post_or_put_sensors = phis.define_post_or_put(getter=phis.get_sensors,
                                              poster=phis.post_sensors,
                                              putter=phis.put_sensors)

sensor_uris = []
for sensor in sensors:
    query_params = {"label": sensor["label"]}
    sensor_uris.append(post_or_put_sensors(session, query_params, sensor)["uri"])

print("URIS: ")
for uri in sensor_uris:
    print(uri)

print()
print("=====================")
print("SENSOR PROFILES:")
print()
print()

# Sensor profile metadata
sensor_profiles = [{'uri': sensor_uris[0],
                   'properties': [{'rdfType': None,
                                      'relation': 'http://www.opensilex.org/vocabulary/oeso#pixelSize',
                                      'value': '0'},
                                     {'rdfType': None,
                                      'relation': 'http://www.opensilex.org/vocabulary/oeso#height',
                                      'value': '1'},
                                     {'rdfType': None,
                                      'relation': 'http://www.opensilex.org/vocabulary/oeso#width',
                                      'value': '1024'}]}]

# Handling sensor profiles is a bit different, because we need to provide
# the URI of the associated sensor as a path parameter to the GET service.
# The other major difference is that there is no PUT service, only a POST
# service. 
def post_sensor_profile(sensor_profile):
    # Firstly, we need to prepare the sensor URI as a path parameter
    # To do this, we use urllib to convert the sensor uri to something
    # that can be used as a path parameter. It will turn something like
    # http://www.opensilex.org/opensilex/2019/s19004
    # into 
    # http%3A%2F%2Fwww.opensilex.org%2Fopensilex%2F2019%2Fs19004
    sensor_uri = urllib.parse.quote(sensor_profile["uri"], safe="")

    # We need to prepare a special getter that will pass the sensor uri
    # to the real getter behind the scenes. That makes the getter compatible
    # with phis.define_post_or_put, which does not support parameters other
    # than query parameters.
    def sensor_profile_getter(session, query_params): 
        result = phis.get_sensor_profiles_uri(session, 
                                              query_params, 
                                              uri=sensor_uri)
        return result
    
    # Given the special getter, we can now define post_or_put. We don't
    # provide a putter, because there is no such service for sensor profiles
    # This means the resulting function will prevent an existing sensor
    # profile from being reposted, but it will not update an existing
    # sensor profile.
    post_if_not_existing = phis.define_post_or_put(getter=sensor_profile_getter,
                            poster=phis.post_sensor_profiles,
                            putter=None)

    return post_if_not_existing(session, {}, sensor_profiles[0])
    
sensor_profile_uris = []
for sensor_profile in sensor_profiles:
    sensor_profile_uris.append(post_sensor_profile(sensor_profile)["uri"])

print("URIS (these will be the same as the sensors):")
for uri in sensor_profile_uris:
    print(uri)
    
