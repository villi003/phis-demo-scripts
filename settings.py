#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Note, aspects of these scripts are based on the example clients provided in
# https://github.com/OpenSILEX/phis-ws-clients/tree/master/python


import os
timezone = 'Europe/Amsterdam'

dataset_directory = "dataset" # Replace with full dataset location
phis_host_url = "http://192.168.56.101:8080/opensilex/rest/" # Note, please make sure the URL ends with a slash

username = 'admin@opensilex.org'
password = 'admin'

