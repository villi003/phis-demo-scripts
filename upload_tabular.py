#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Note, aspects of these scripts are based on the example clients provided in
# https://github.com/OpenSILEX/phis-ws-clients/tree/master/python

import requests, json, csv, os.path, hashlib, pytz
import ntpath
import mimetypes
import datetime
import urllib
import pprint
import phis
import numpy as np
import pandas as pd
import pytz
import create_groups
import create_provenance
import create_scientific_objects

# PHIS needs a correctly formatted time zone. See function fix_timezone_portion
# in the below code to see how to fix isoformat versions of datetimes
timezone = 'Europe/Amsterdam'
timezone_obj = pytz.timezone(timezone)

def localize_datetime(dt):
    return timezone_obj.localize(dt)

phis_host_url = "http://192.168.56.101:8080/opensilex/rest/" # Note, please make sure the URL ends with a slash

group_uri = create_groups.group_uri
scientific_object_type ="http://www.opensilex.org/vocabulary/oeso#Plant"

username = 'admin@opensilex.org'
password = 'admin'

def make_data_frame():
    # This function makes a Pandas data frame. This is essentially a 
    # spreadsheet object for the Python ecosystem. R users can see this
    # as corresponding to R's data frame structure. The data frame will
    # contain three columns containing the sample ids, the scientific
    # object iris, and the corresponding disease state.
    # Please have a look at the result of print(dataframe) later in the
    # script to get a feeling for what the dataframe contains.
    label_iri_dict = create_scientific_objects.scientific_object_iris
    labels = []
    iris = []
    class_labels = []
    for label in label_iri_dict.keys():
        labels.append(label)
        iris.append(label_iri_dict[label])
        # Below we build the data labels (diseased or healthy) for each
        # of the scientific objects
        sample_code = label.split("_")[-1]
        if (sample_code[0] == "D"):
            class_labels.append("diseased")
        elif (sample_code[0] == "C"):
            class_labels.append("healthy")
        else:
            raise RuntimeError("Sample code should either start with a D or a C, encountered " + sample_code[0] + " instead.")
    # Now that we have all the sample ids, iris and class labels, we can
    # create the data frame.
    return pd.DataFrame.from_dict({"sample_id" : labels,
                                   "scientific_object_iri" : iris,
                                   "is_diseased" : class_labels})

dataframe = make_data_frame()

# Print the data frame for the user to have a look at
print(dataframe)

session = phis.create_session(phis_host_url, username, password)

# In create_project, we explicitly did the check for existing projects, in
# the below call, we shorten this procedure by using the method define_post_or_put
# implemented in phis.py . A call to this post_or_put_trait/method/unit/variable should now
# be interpreted just like the larger procedure in create_project.py
# The interested reader should refer to define_post_or_put_experiment in
# phis.py. However, the reader may safely simply note that the general procedure
# of create_project.py is being followed.

post_or_put_trait = phis.define_post_or_put(getter=phis.get_traits,
                                            poster=phis.post_traits,
                                            putter=phis.put_traits)

post_or_put_method = phis.define_post_or_put(getter=phis.get_methods,
                                            poster=phis.post_methods,
                                            putter=phis.put_methods)

post_or_put_unit = phis.define_post_or_put(getter=phis.get_units,
                                            poster=phis.post_units,
                                            putter=phis.put_units)

post_or_put_variable = phis.define_post_or_put(getter=phis.get_variables,
                                            poster=phis.post_variables,
                                            putter=phis.put_variables)


# Trait metadata
trait_data = {
    "label": "Diseased",
    "comment": "Whether or not the subject in question is diseased."
  }

# Method metadata
method_data = {
    "label": "direct_observation",
    "comment": "Value is assigned by a human through direct observation."
  }

# Unit metadata
unit_data = {
    "label": "dimensionless",
    "comment": "arbitrary units"
  }

the_trait=post_or_put_trait(session, 
                  trait_data, 
                  trait_data, 
                  verbose=True)

the_method=post_or_put_method(session, 
                   method_data, 
                   method_data, 
                   verbose=True)

the_unit=post_or_put_unit(session, 
                 unit_data, 
                 unit_data, 
                 verbose=True)

# Variable metadata, note we use the trait, method and unit URIs
variable_data = {
    "label": "Diseased",
    "comment": "Does the scientific object carry a disease?",
    "trait": the_trait["uri"],
    "method": the_method["uri"],
    "unit": the_unit["uri"]
  }

the_variable=post_or_put_variable(session, 
                     {"label" : variable_data["label"]}, 
                     variable_data, 
                     verbose=True)

def phis_post_data(session, provenance_uri, object_uri, variable_uri, date_and_time, value):
    # We define the web request call for POST data/ . Note that 
    # define_post_or_put is not applicable, since there is no PUT method
    # for this service.
    phis_post_data = phis.define_phis_request("POST", "data")
    # Data point metadata
    data_dict = [
      {
        "provenanceUri": provenance_uri,
        "objectUri": object_uri,
        "variableUri": variable_uri,
        "date": date_and_time,
        "value": value
      }
    ]
    
    return phis_post_data(session, data = data_dict)
    
def post_all_data():    
    var_uri = the_variable["uri"]
    date_string = "2019-06-15T12:00:00+0200"
    object_iris = dataframe["scientific_object_iri"]
    diseaseds = dataframe["is_diseased"]

    # Note that, if a data point already exists, its value will
    # NOT be changed by running the following code.
    for object_iri, diseased in zip(object_iris, diseaseds):
        # Here we simply take each object IRI and its corresponding
        # disease state. Along with the provenance, variable and date,
        # these uniquely define the datapoint. 
        result = phis_post_data(session, 
                       create_provenance.provenance_uri, 
                       object_iri,
                       var_uri,
                       date_string,
                       diseased)  

post_all_data()
